<?php

/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 */
?>
<!doctype html>
<html <?php language_attributes(); ?>>

<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
    <?php wp_body_open(); ?>

    <header id="header">
        <div id="topbar" class="topbar">
            <div class="container d-flex justify-content-between">
                <?php
                wp_nav_menu( array(
                    'theme_location' => 'topbar_menu_left',
                    'depth' => 1,
                    'container_class' => 'collapse navbar-collapse',
                    'menu_class' => 'navbar-nav left',
                    'fallback_cb' => 'WP_Bootstrap_Navwalker::fallback',
                    'walker' => new WP_Bootstrap_Navwalker()
                ) );                
                wp_nav_menu( array(
                    'theme_location' => 'topbar_menu_right',
                    'depth' => 1,
                    'container_class' => 'collapse navbar-collapse',
                    'menu_class' => 'navbar-nav right',
                    'fallback_cb' => 'WP_Bootstrap_Navwalker::fallback',
                    'walker' => new WP_Bootstrap_Navwalker()
                ) );
                ?>
            </div>
            </div>
        </div>        
        <nav id="navbar" class="navbar container">
            <?php
            if ( function_exists( 'the_custom_logo' ) && has_custom_logo() ) {
                the_custom_logo();
            } else {
                echo '<h1>' . get_bloginfo( 'title' ) . '</h1>';
            }
            ?>

            <button type="button" class="navbar-toggler" data-target="#navbar-nav">
                <span class="navbar-toggler-icon"></span>             
            </button>

            <?php
            wp_nav_menu( array(
                'theme_location' => 'main_menu',
                'depth' => 2,
                'container' => 'div',
                'container_class' => 'collapse navbar-collapse',
                'container_id' => 'navbar-nav',
                'menu_class' => 'navbar-nav has-dropdown-menu-to-left',
                'fallback_cb' => 'WP_Bootstrap_Navwalker::fallback',
                'walker' => new WP_Bootstrap_Navwalker()
            ) );
            ?>
        </nav>        
    </header>