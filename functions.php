<?php

/**
 * Functions and Definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 */

/**
 * Enqueuing of scripts and styles.
 */
function myhauz_scripts() {
    wp_enqueue_style( 'myhauz-webfonts-styles', get_template_directory_uri() . '/assets/css/shared/webfonts.css', array(), wp_get_theme()->get( 'Version' ) );
    wp_enqueue_style( 'myhauz-shared-styles', get_template_directory_uri() . '/assets/css/shared/shared-styles.css', array(), wp_get_theme()->get( 'Version' ) );
    wp_enqueue_style( 'myhauz-frontend-styles', get_template_directory_uri() . '/assets/css/frontend/styles.css', array(), wp_get_theme()->get( 'Version' ) );    

    wp_enqueue_script( 'myhauz-frontend-script', get_template_directory_uri() . '/assets/js/frontend/frontend-scripts.js', array( 'jquery' ), wp_get_theme()->get( 'Version' ), true );
}

add_action('wp_enqueue_scripts', 'myhauz_scripts');

/**
 * Gutenberg scripts
 * @see https://www.billerickson.net/block-styles-in-gutenberg/
 */
function myhauz_gutenberg_scripts() {
    wp_enqueue_style( 'myhauz-webfonts-styles', get_template_directory_uri() . '/assets/css/shared/webfonts.css', array(), wp_get_theme()->get( 'Version' ) );
    wp_enqueue_script( 'myhauz-editor-script', get_stylesheet_directory_uri() . '/assets/js/admin/editor.js', array( 'wp-blocks', 'wp-dom' ), '1.0', true );
}

add_action('enqueue_block_editor_assets', 'myhauz_gutenberg_scripts');


/** 
 * Set theme defaults and register support for various WordPress features.
 */
function myhauz_setup() {
    // Enabling translation support
    $textdomain = 'myhauz';
    load_theme_textdomain( $textdomain, get_stylesheet_directory() . '/languages/' );
    load_theme_textdomain( $textdomain, get_template_directory() . '/languages/' );

    // Customizable logo
    add_theme_support( 'custom-logo', array(
        'height'      => 37,
        'width'       => 256,
        'flex-height' => true,
        'flex-width'  => true,
        'header-text' => array( 'site-title', 'site-description' ),
    ) );

    // Menu registration
    register_nav_menus( array(
        'topbar_menu_left' => __( 'Topbar Left', 'myhauz' ),
        'topbar_menu_right' => __( 'Topbar Right', 'myhauz' ),
        'main_menu' => __( 'Main Menu', 'myhauz' ),
    ) );

    // Let WordPress manage the document title.
    add_theme_support( 'title-tag' );
    
    // Enable support for featured image on posts and pages.		 	
    add_theme_support( 'post-thumbnails' );

    // Load custom styles in the editor.
    add_theme_support( 'editor-styles' );
    add_editor_style( get_stylesheet_directory_uri() . '/assets/css/shared/shared-styles.css' );

    // Enable support for embedded media for full weight
    add_theme_support( 'responsive-embeds' );

    // Enables wide and full dimensions
    add_theme_support( 'align-wide' );

    // Standard style for each block.
	add_theme_support( 'wp-block-styles' );

    // Disable custom colors
    add_theme_support( 'disable-custom-colors' );

    // Creates the specific color palette
    add_theme_support('editor-color-palette', array(
        array(
            'name'  => __( 'White', 'myhauz' ),
            'slug'  => 'white',
            'color'    => '#ffffff',
        ),        
        array(
            'name'  => __( 'Ocean Green', 'myhauz' ),
            'slug'  => 'ocean-green',
            'color'    => '#5bc2a7',
        ),
        array(
            'name'  => __( 'Deep Saffron', 'myhauz' ),
            'slug'  => 'deep-saffron',
            'color'    => '#ffa037',
        ),
        array(
            'name'  => __( 'Dim Gray', 'myhauz' ),
            'slug'  => 'dim-gray',
            'color'    => '#666666',
        ),
        array(
            'name'  => __( 'Eerie Black', 'myhauz' ),
            'slug'  => 'eerie-black',
            'color'    => '#222222',
        ),
    ));

    // Disable custom gradients
    add_theme_support( 'disable-custom-gradients' );
}

add_action( 'after_setup_theme', 'myhauz_setup' );

/**
 * Registration of widget areas.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function myhauz_sidebars() {
    // Args used in all calls register_sidebar().
    $shared_args = array(
        'before_title' => '<h4 class="widget-title">',
        'after_title' => '</h4>',
        'before_widget' => '<div class="widget %2$s"><div class="widget-content">',
        'after_widget' => '</div></div>',
    );
    
    // Footer #1
    register_sidebar( array_merge( $shared_args, array(
        'name' => __( 'Footer #1', 'myhauz' ),
        'id' => 'myhauz-sidebar-footer-1',
        'description' => __( 'The widgets in this area will be displayed in the first column in Footer.', 'myhauz' ),
    ) ) );

    // Footer #2
    register_sidebar( array_merge( $shared_args, array(
        'name' => __( 'Footer #2', 'myhauz' ),
        'id' => 'myhauz-sidebar-footer-2',
        'description' => __( 'The widgets in this area will be displayed in the second column in Footer.', 'myhauz' ),
    ) ) );

    // Footer #3
    register_sidebar(array_merge($shared_args, array(
        'name' => __( 'Footer #3', 'myhauz' ),
        'id' => 'myhauz-sidebar-footer-3',
        'description' => __( 'The widgets in this area will be displayed in the third column in Footer.', 'myhauz' ),
    )));

    // Footer #4
    register_sidebar(array_merge($shared_args, array(
        'name' => __( 'Footer #4', 'myhauz' ),
        'id' => 'myhauz-sidebar-footer-4',
        'description' => __( 'The widgets in this area will be displayed in the fourth column in Footer.', 'myhauz' ),
    )));
}

add_action( 'widgets_init', 'myhauz_sidebars' );

/**
 *  WordPress Bootstrap Nav Walker
 */
require_once get_template_directory() . '/includes/classes/class-wp-bootstrap-navwalker.php';
