<?php
/**
 * The template for displaying the footer
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 */
?>

<footer id="footer">
    <div class="footer-widgets">
        <div class="container">
            <div>
                <?php if ( is_active_sidebar( 'myhauz-sidebar-footer-1' ) ) dynamic_sidebar( 'myhauz-sidebar-footer-1' ); ?>
            </div>

            <div>
                <?php if ( is_active_sidebar( 'myhauz-sidebar-footer-2' ) ) dynamic_sidebar( 'myhauz-sidebar-footer-2' ); ?>
            </div>

            <div>
                <?php if ( is_active_sidebar( 'myhauz-sidebar-footer-3' ) ) dynamic_sidebar( 'myhauz-sidebar-footer-3' ); ?>
            </div>

            <div>
                <?php if ( is_active_sidebar( 'myhauz-sidebar-footer-4' ) ) dynamic_sidebar( 'myhauz-sidebar-footer-4' ); ?>
            </div>
        </div>
    </div>
    <div class="footer-copyright">
        <div class="container">
            <span class="text-white">&copy; <?php echo bloginfo( 'title' ) . ', ' . wp_date( 'Y' ) . ' - ' . __( 'All rights reserved', 'myhauz' ); ?></span>
            <span class="text-white"><?php _e( 'Design by', 'myhauz' ); ?> <a href="http://www.stratesign.com.br/" target="_blank" rel="noopener">Stratesign</a></span>
        </div>
    </div>
</footer>

<?php wp_footer(); ?>

</body>

</html>
